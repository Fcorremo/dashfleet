<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id'); // Cambiado a unsignedBigInteger
            $table->foreign('customer_id')->references('doc_id')->on('customers'); // Referencia la columna 'id' en 'customers'
            $table->string('delivery_address');
            $table->string('order_status')->default('pending'); 
            $table->date('estimated_delivery_date');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
