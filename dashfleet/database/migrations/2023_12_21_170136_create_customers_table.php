<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id(); 
            $table->string('document_type'); 
            $table->string('doc_id')->unique();
            $table->string('customer_name');
            $table->string('customer_address');
            $table->string('customer_phone');

            // Auditoría
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
