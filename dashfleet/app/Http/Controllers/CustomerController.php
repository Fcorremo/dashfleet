<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    public function create(Request $request)
    {
        // create customer
        $customer = Customer::create($request->all());

        return response()->json($customer, 201);
    }

    public function update(Request $request, $doc_id)
    {
        // update customer
        $customer = Customer::where('doc_id', $doc_id)->first();

        if ($customer) {
            $customer->update($request->all());
            return response()->json($customer, 200);
        } else {
            return response()->json(['message' => 'Cliente no encontrado'], 404);
        }
    }

    public function show($doc_id)
    {
        // list by id
        $customer = Customer::where('doc_id', $doc_id)->first();

        if ($customer) {
            return response()->json($customer, 200);
        } else {
            return response()->json(['message' => 'Cliente no encontrado'], 404);
        }
    }

    public function index()
    {
        // list all
        $customers = Customer::all();

        return response()->json($customers, 200);
    }

    public function destroy($doc_id)
    {
        // delete by id
        $customer = Customer::where('doc_id', $doc_id)->first();

        if ($customer) {
            $customer->delete();
            return response()->json(['message' => 'Cliente eliminado correctamente'], 200);
        } else {
            return response()->json(['message' => 'Cliente no encontrado'], 404);
        }
    }

    public function showClientsView()
{
    $clients = Customer::all();
    return view('clients', compact('clients'));
}


}
