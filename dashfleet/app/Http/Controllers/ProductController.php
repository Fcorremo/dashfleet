<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product; 

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return response()->json($products, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required|string',
            'product_reference' => 'required|string|unique:products',
            'price' => 'required|numeric',
        ]);

        $product = Product::create([
            'product_name' => $request->input('product_name'),
            'product_reference' => $request->input('product_reference'),
            'price' => $request->input('price'),
        ]);

        return response()->json($product, 201);
    }


    
}
