<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderItem;

class OrderController extends Controller
{
    public function createOrderWithProducts(Request $request)
    {
        try {
            $request->validate([
                'customer_id' => 'required|exists:customers,doc_id',
                'delivery_address' => 'required|string',
                'estimated_delivery_date' => 'required|date',
                'order_items' => 'required|array',
                'order_items.*.product_id' => 'required|exists:products,id',
                'order_items.*.quantity' => 'required|integer|min:1',
            ]);

            
            $order = Order::create([
                'customer_id' => $request->input('customer_id'),
                'delivery_address' => $request->input('delivery_address'),
                'estimated_delivery_date' => $request->input('estimated_delivery_date'),
            ]);

          
            foreach ($request->input('order_items') as $item) {
                OrderItem::create([
                    'order_id' => $order->id,
                    'product_id' => $item['product_id'],
                    'quantity' => $item['quantity'],
                ]);
            }

            return response()->json($order, 201);
        } catch (\Illuminate\Validation\ValidationException $e) {
 
            \Log::error('Validation error: ' . json_encode($e->errors()));

       
            return response()->json(['error' => 'Error de validación', 'details' => $e->errors()], 422);
        } catch (\Exception $e) {
     
            \Log::error($e);

           
            return response()->json(['error' => 'Error al procesar la solicitud'], 500);
        }
    }

    public function getOrderById($orderId)
    {
        try {
            $order = Order::with('orderItems.product')->findOrFail($orderId);

            return response()->json($order, 200);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Orden no encontrada'], 404);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['error' => 'Error al procesar la solicitud'], 500);
        }
    }

    public function getAllOrders()
    {
        try {
            $orders = Order::with('orderItems.product')->get();

            return response()->json($orders, 200);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['error' => 'Error al procesar la solicitud'], 500);
        }
    }


}


