<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_name',
        'product_reference',
        'price',
    ];

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }
}
