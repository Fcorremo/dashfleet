<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'delivery_address',
        'order_status',
        'estimated_delivery_date',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'doc_id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }
}

