<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = [
        'document_type',
        'doc_id',
        'customer_name',
        'customer_address',
        'customer_phone',
    ];
    
    public function orders()
    {
        return $this->hasMany(Order::class, 'doc_id', 'customer_id');
    }
}
