<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/clients', [CustomerController::class, 'showClientsView']);