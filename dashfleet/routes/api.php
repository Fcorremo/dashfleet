<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;


// Customer Routes
Route::post('/customers', [CustomerController::class, 'create']);
Route::put('/customers/{doc_id}', [CustomerController::class, 'update']);
Route::get('/customers/{doc_id}', [CustomerController::class, 'show']);
Route::get('/customers', [CustomerController::class, 'index']);
Route::delete('/customers/{doc_id}', [CustomerController::class, 'destroy']);

// Product Routes
Route::get('/products', [ProductController::class, 'index']);
Route::post('/products', [ProductController::class, 'store']);

//Orders
Route::post('/orders/create-with-products', [OrderController::class, 'createOrderWithProducts']);
Route::get('/orders/{orderId}', [OrderController::class, 'getOrderById']);
Route::get('/orders', [OrderController::class, 'getAllOrders']);


