import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './components/App.vue';

const routes = [
  { path: '/productos', component: require('./components/Productos.vue').default },
  { path: '/clientes', component: require('./components/Clientes.vue').default },
  { path: '/pedidos', component: require('./components/Pedidos.vue').default },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

createApp(App).use(router).mount('#app');
