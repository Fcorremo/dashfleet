<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your App Title</title>
    <!-- Aquí puedes incluir tus estilos CSS o enlaces a CDN de Bootstrap si los estás utilizando -->
</head>
<body>

    <div class="container">
        @yield('content')
    </div>

</body>
</html>
