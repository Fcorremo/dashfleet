@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Listado de Clientes</h2>
                <ul>
                    @foreach($clients as $client)
                        <li>{{ $client->name }} - {{ $client->email }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
                <h2>Agregar Nuevo Cliente</h2>
                <form method="post" action="{{ url('/clients/create') }}">
                    @csrf
                    <label for="name">Nombre:</label>
                    <input type="text" name="name" required>
                    
                    <label for="email">Correo electrónico:</label>
                    <input type="email" name="email" required>
                    
                    <button type="submit">Agregar Cliente</button>
                </form>
            </div>
        </div>
    </div>
@endsection
