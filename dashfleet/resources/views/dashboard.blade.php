<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <!-- Agrega los enlaces a los estilos de Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <!-- Barra lateral -->
        <nav id="sidebar" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
            <div class="position-sticky">
                <ul class="nav flex-column">
                    <!-- Menú Clientes -->
                    <li class="nav-item">
                        <a class="nav-link" href="#clientes">
                            Clientes
                        </a>
                        <ul class="nav flex-column ml-3">
                            <li class="nav-item">
                                <a class="nav-link" href="#crear-cliente">Crear Cliente</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#listar-clientes">Listar Clientes</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Menú Productos -->
                    <li class="nav-item">
                        <a class="nav-link" href="#productos">
                            Productos
                        </a>
                        <ul class="nav flex-column ml-3">
                            <li class="nav-item">
                                <a class="nav-link" href="#crear-producto">Crear Producto</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#listar-productos">Listar Productos</a>
                            </li>
                        </ul>
                    </li>
                    <!-- Menú Pedidos -->
                    <li class="nav-item">
                        <a class="nav-link" href="#pedidos">
                            Pedidos
                        </a>
                        <!-- Puedes agregar submenús de Pedidos si es necesario -->
                    </li>
                </ul>
            </div>
        </nav>

        <!-- Contenido principal -->
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <!-- Contenido de la página va aquí -->
            <h2 class="mt-2">Bienvenido al Dashboard</h2>
            <!-- Puedes incluir más contenido según sea necesario -->
        </main>
    </div>
</div>

<!-- Agrega los scripts de Bootstrap al final del cuerpo -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
